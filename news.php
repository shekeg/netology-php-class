<?php
  require_once(__DIR__ . '/classes.php');
  $news = new News();
?>

<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Новости</title>
</head>
<body>
  <h1>Новости</h1>
  <a href="addNews.php">Добавить новость</a>
  <?php $news->showNewsList() ?>
</body>
</html>