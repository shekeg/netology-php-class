<?php

  require_once(__DIR__ . '/functions.php');

  class News {
    
    private $title;
    private $date;
    private $text;

    public function setNews($title, $text) {
      $this->title = $title;
      $this->date = $date = date('d.m.Y h:i', time());;
      $this->text = $text;
    }

    public function saveNews($filePath) {
      $fileData = file_get_contents($filePath);
      $createdNews = json_decode($fileData, true);
      unset($fileData);
      $news = (array)$this;
      foreach ($news as $key => $value) {
        unset($news[$key]);
        $key = str_replace('News', '', $key);
        $news[$key] = $value;
      }
      $createdNews[] = $news;
      $jsonNews = str_replace('\u0000', '', json_encode($createdNews, JSON_UNESCAPED_UNICODE));
      file_put_contents($filePath, $jsonNews);
    }

    private function getNewsList() {
      $fileData = file_get_contents(__DIR__ . '/news.json');
      return json_decode($fileData, true);
    }

    private function getComments($comments) {
      echo "<h2>Комментарии</h2>";
      foreach($comments as $comment) {
        $commentObj = new Comments();
        $commentObj->setText($comment);
        echo "<p>{$commentObj->getText()}</p>";
      }
    }

    public function showNewsList() {
      $newsList = $this->getNewsList();
      foreach($newsList as $news) {
        $this->title = checkData($news['title'], 'Не удалось получить заголовок');
        $this->date = checkData($news['date'], 'Не удалось получить дату');
        $this->text = checkData($news['text'], 'Не удалось получить текст');
        echo "<h2>{$this->title} ({$this->date})</h2>";
        echo "<p>Текст: {$this->text}</p>";
        if (!empty($news['comments'])) {
          $this->getComments($news['comments']);
        } else {
          echo '<p>Нет комментариев<p>';
        }
      }
    }
  }

  class Comments {
    private $text;

    public function setText($text) {
      $this->text = $text;
    }

    public function getText() {
      return $this->text;
    }
  }