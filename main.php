<?php

  class Goods {
    private $price = 0;

    function __construct() {
      echo '<pre>';
        var_dump($this);
      echo '</pre>';
    }

    public function setPrice($price) {
      $this->price = $price;
      return $this;
    }
  }

  class Car extends Goods{
    private $speed;

    function __construct($speed) {
      $this->speed = $speed;
      parent::__construct();
    }

    public function go() {
      $speed = $this->speed;
      echo "Машине едет со скоростью {$speed}";
    }
  }


  class TV extends Goods{
    private $channel;
    private $producer;

    function __construct($producer, $channel) {
      $this->producer = $producer;
      $this->channel = $channel; 
      parent::__construct();
    }

    public function changeChannel($channel) {
      $this->channel = $channel;
      echo "Выбранный канал {$channel}";
    }
  }


  class BallPen extends Goods{
    private $color;

    function __construct($color) {
      if ($color === 'blue' or $color === 'red' or $color === 'black')  {
        $this->color = $color;
      }
      parent::__construct();
    }

    function write($text) {
      if (!empty($this->color)) {
        $color = $this->color;
      } else {
        die('Поддерживаемые цвета: blue, red, black');
      }
      $fontSize = 30;
      $width = 750;
      $height = 40;

      $image = imagecreate($width, $height);
      imagecolorallocate($image, 255, 255, 255);
      switch ($color) {
        case 'blue':
          $textColor = imagecolorallocate($image, 0, 0, 255);
          break;
        case 'red':
          $textColor = imagecolorallocate($image, 255, 0, 0);
          break;
        case 'black':
          $textColor = imagecolorallocate($image, 0, 0, 0);
          break;
      }
      imagettftext($image, $fontSize, 0, 15, 35, $textColor, __DIR__ . '/fonts/OpenSans.ttf', $text);

      header('Content-type: image/png');
      imagepng($image);
    }
  }


  class Duck extends Goods {
    private $soundSource;
    
    function __construct($soundSource) {
      $this->soundSource = $soundSource;
      parent::__construct();
    }

    public function getSound() {
      echo "<audio autoplay> <source src=\"{$this->soundSource}\" type=\"audio/mpeg\"> </audio>"; 
    }
  }

  $dress = new Goods();
  $phone = new Goods();

  $opel = new Car(190);
  $bmw = new Car(200);

  $samsung = new TV('Samsung', 1);
  $lg = new TV('LG', 2);

  $ballPenBlack = new BallPen('black');
  $ballPenRed = new BallPen('red');

  $duck1 = new Duck('https://www.google.com/logos/fnbx/animal_sounds/duck.mp3');
  $duck2 = new Duck('https://www.google.com/logos/fnbx/animal_sounds/duck.mp3');
