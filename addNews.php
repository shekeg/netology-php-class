<?php
  require_once(__DIR__ . '/classes.php');

  if (!empty($_POST)) {
    $news = new News();
    $news->setNews($_POST['title'], $_POST['text']);
    $news->saveNews(__DIR__ . '/news.json');
    header('Location: news.php');
    die();
  }
?>

<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Добавление новости</title>
</head>
<body>
  <h1>Создание новости</h1>
  <form action="addNews.php" method="POST">
    <p>
      <label for="title">Заголовок для новости</label>
      <input type="text" name="title" id="title">
    </p>
    <p>
      <label for="text">Текст новости</label>
      <textarea rows="10" cols="45" id="text" name="text"></textarea>
    </p>
    <p>
      <input type="submit" value="Создать">
    </p>
  </form>
</body>
</html>